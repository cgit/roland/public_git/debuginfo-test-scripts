#!/bin/sh

case "$1" in
all) names='-name exec -o -name dyn -o -name rel' ;;
norel) names='-name exec -o -name dyn' ;;
*) names="-name $1" ;;
esac
shift

depth=1
[ $# -gt 0 ] || { depth=2; set .; }

find "$@" -mindepth $depth -maxdepth $depth \( $names \) ! -size 0 -print |
while read f; do
  f="${f#./}"
  d="${f%/*}"
  sed "s@^@$d/@" $f
done
