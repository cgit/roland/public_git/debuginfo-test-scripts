#!/usr/bin/make -srf

summary := $(wildcard */summary)

which ?= all

ifeq (,$(summary))

dirs := $(wildcard */)

ifeq (,$(dirs))
$(which).list:
	@echo "`pwd` not good"; exit 2
else
$(which).list: %: $(dirs:=$(which).list)
	cat -- $^ > $@.new
	mv -f $@.new $@
endif

me := $(MAKEFILE_LIST)

FORCE:;
%/$(which).list: FORCE
	$(MAKE) -f `(cd $(dir $(me)); pwd)`/$(notdir $(me)) \
		-C $(@D) which=$(which)

else

$(which).list: %.list: $(summary)
	(echo 'all:\'; \
	 list-files.sh $* | sed 's,^.*$$,test@/&\\,' | sort -R; \
	 echo) > $@.new
	mv -f $@.new $@

endif
