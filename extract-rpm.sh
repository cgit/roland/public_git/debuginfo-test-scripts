#!/bin/sh

v=0
if [ "x$1" = "x-v" ]; then
  v=1
  shift
fi

dest=$1; shift

while read file; do

  name="${file%.rpm}"
  if [ "$name" = "$file" ]; then
    [ $v -eq 0 ] || echo >&2 "not .rpm: $file"
    continue
  fi
  rpm="${name##*/}"

  archless=${rpm%.*}
  arch=${rpm##*.}
  d=${arch}/${archless:0:2}/${archless:2:2}

  d="$dest/$d"
  if [ -d "$d/$rpm" ] && ! rmdir "$d/$rpm" 2> /dev/null; then
    [ $v -eq 0 ] || echo >&2 "$rpm already there"
    continue
  fi
  mkdir -p "$d/$rpm" || exit
  rpm2cpio "$file" |
  (cd "$d/$rpm"; cpio --quiet --extract --no-abs --make-dir ||
  echo >&2 "FAILED: $rpm")

  echo "$rpm"

done
