#!/bin/sh

srcdir=`dirname $0`
srcdir=`(cd "$srcdir"; pwd)`
PATH="$srcdir:$PATH"
export PATH
ulimit -c unlimited

which=$1
shift

run_all()
{
  set -e
  update-file-lists.mk which=${which} -j$(getconf _NPROCESSORS_ONLN)

  # The spawn of make run copies that do run_one(), below.
  make -s -j$(getconf _NPROCESSORS_ONLN) -r -f ${which}.list -f - <<EOF
test@/%:; $0 subr \$* $*
EOF
}

run_one()
{
  file=$1
  shift

  dir=${file//\//:}
  name=${file##*/}

  rpm=${file%%/*}
  archless=${rpm%.*}
  arch=${rpm##*.}
  d=${arch}/${archless:0:2}/${archless:2:2}

  mkdir -p $work/$dir &&
  bunzip2 < $d/$file.bz2 > $work/$dir/$name &&
  echo "${*//{\}/$name}" > $work/$dir/cmd && {

    (cd $work/$dir && . cmd) > $work/$dir/log 2>&1
    status=$?
    echo $status > $work/$dir/status

    if [ $status -ne 0 ] || [ -s $work/$dir/log ]; then
      # Test failed, leave everything there.
      echo "FAIL ($status) $file $dir"
      return 0
    fi
    # Look for excess files.  Consider it failed if there are any.
    ls -A $work/$dir | fgrep -q -v "$name
cmd
log
status" && { echo "FAIL (files) $file $dir"; return 0; }

    # Fully clean test, erase the evidence.
    rm -rf $work/$dir
  }
}

if [ "$which" = subr ]; then
  run_one "$@"
else
  work=$1
  shift
  export work
  run_all "$@"
fi
