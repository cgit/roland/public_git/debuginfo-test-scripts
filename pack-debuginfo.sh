#!/bin/sh

unset MAKEFLAGS MFLAGS

get_size()
{
  size="$1->$3"
}

do_src()
{
  if was=`du -sh usr/src 2> /dev/null`; then
    tar cf - usr/src | bzip2 -9 > src.tar.bz2
    is=`du -sh src.tar.bz2`
    rm -rf usr/src &
    get_size $was $is
  else
    size="missing"
  fi
  printf "src %s" "$size" > summary
}

do_collect()
{
  > exec
  > dyn
  > rel
  > archive
  find usr/lib* -type f -print0 | xargs --no-run-if-empty -0 file -N -F ' ' -i |
  awk -v dir="$dir" -v src="$size" '
BEGIN { e = d = r = u = a = 0 }
NF > 2 && $2 ~ "application/x-" { sub(/[;,] .*$/, "") }
NF == 2 && $2 == "application/x-executable" { print $1 > "exec" ; ++e; next }
NF == 2 && $2 == "application/x-sharedlib" { print $1 > "dyn" ; ++d; next }
NF == 2 && $2 == "application/x-object" { print $1 > "rel" ; ++r; next }
NF == 2 && $2 == "application/x-archive" { print $1 > "archive" ; ++a; next }
{ print "unexpected line from", dir ": " $0 > "/dev/stderr";
  ++u; print $0 > "bogon"; next }
END {
  printf "exec %d dyn %d rel %d archive %d", e, d, r, a;
  if (u) printf " bogons %d", u;
  print ""
}'
}

do_dir()
{
  do_src &

  find usr/lib/debug -type f ! -links 1 -exec rm -f {} \; 2> /dev/null

  if was=`du -sh usr/lib/debug 2> /dev/null`; then
    summary=`do_collect`
    (echo 'all: \'
     sed 's,$,.bz2,;$!s,$,\\,' exec dyn rel
     echo '%.bz2: %; bzip2 -9 $<'
    ) | make -s -r -f - -j`getconf _NPROCESSORS_ONLN`
    is=`du -sh usr/lib/debug`
    get_size $was $is
    summary="$summary bz2 $size"
  else
    summary="debug missing"
  fi

  wait

  echo " $summary" >> summary

  echo "packed $dir"
}

while read rpm; do

  archless=${rpm%.*}
  arch=${rpm##*.}
  d=${arch}/${archless:0:2}/${archless:2:2}/$rpm

  if [ -r "$d/summary" ]; then
    echo >&2 "$rpm looks packed"
    continue
  fi

  dir=$rpm
  (cd "$d" && do_dir)

done

wait
