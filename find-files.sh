#!/bin/sh

while read dir; do
  find "$dir" -name '*.debug' -type f -print0 |
  xargs -0 file -N -F ' ' -i |
  awk '
NF == 2 && $2 == "application/x-executable" { print $1 >> "exec" ; next }
NF == 2 && $2 == "application/x-sharedlib" { print $1 >> "dyn" ; next }
NF == 2 && $2 == "application/x-object" { print $1 >> "rel" ; next }
NF == 2 && $2 == "application/x-archive" { print $1 >> "archive" ; next }
{ print "unexpected line:", $0; next }
'
done
