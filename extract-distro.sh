#!/bin/sh

srcdir=`dirname $0`
srcdir=`(cd "$srcdir"; pwd)`
PATH="$srcdir:$PATH"

here=`pwd`

df -h .

find "$@" \( \( -name debug -o -name Debuginfo \) -print -prune \) -o \
     	  \( -name os -prune \) | (
  echo 'all: \'
  sed 's,^.*$,do/@& \\,'
  echo
  echo 'do/@%:;(cd $* && ls | extract-rpm.sh "$$here") | pack-debuginfo.sh'
) | make -f - -r here="$here" -s -j$(getconf _NPROCESSORS_ONLN)

df -h .
